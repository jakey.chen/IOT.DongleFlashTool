﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace IOT.DongleFlashTool.Model
{
    /// <summary>
    /// 命令方法枚举
    /// </summary>
    public enum CommandEnum : byte
    {
        [Description("空值")]
        None = 0x00,

        [Description("开启/关闭 加网")]
        JoinNetwork = 0x06,

        [Description("获取MAC")]
        GetMac = 0x0D,

        [Description("Image Notify")]
        ImageNotify = 0x11,

        [Description("设备正在烧录返回")]
        OTAing = 0x14,

        [Description("OTA完毕")]
        OTAfinished = 0x17
    }
}
