﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOT.DongleFlashTool.Model
{
    public class SendDataBytes
    {
        /// <summary>
        /// 包头字节
        /// </summary>
        private byte _PackageHeadByte = 0x7E;
        public byte PackageHeadByte
        {
            get { return _PackageHeadByte; }
            set { _PackageHeadByte = value; }
        }

        /// <summary>
        /// 包尾字节
        /// </summary>
        private byte _PackageFootByte = 0x7E;
        public byte PackageFootByte
        {
            get { return _PackageFootByte; }
            set { _PackageFootByte = value; }
        }

        ///// <summary>
        ///// 下行标志字节
        ///// </summary>
        //private byte _PackageOutFlagByte = 0xFA;
        //public byte PackageOutFlagByte
        //{
        //    get { return _PackageOutFlagByte; }
        //    set { _PackageOutFlagByte = value; }
        //}

        /// <summary>
        /// 命令字节
        /// </summary>
        private CommandEnum _PackageCommandByte = CommandEnum.None;
        public CommandEnum PackageCommandByte
        {
            get { return _PackageCommandByte; }
            set { _PackageCommandByte = value; }
        }

        /// <summary>
        /// 数据长度字节(低位)
        /// </summary>
        private byte _PackageLengthLowByte = 0x00;
        public byte PackageLengthLowByte
        {
            get { return _PackageLengthLowByte; }
            set { _PackageLengthLowByte = value; }
        }

        /// <summary>
        /// 数据长度字节(高位)
        /// </summary>
        private byte _PackageLengthHighByte = 0x00;
        public byte PackageLengthHighByte
        {
            get { return _PackageLengthHighByte; }
            set { _PackageLengthHighByte = value; }
        }

        /// <summary>
        /// 数据包字节数组
        /// </summary>
        private byte[] _PackageDataBytes = null;
        public byte[] PackageDataBytes
        {
            get { return _PackageDataBytes; }
            set { _PackageDataBytes = value; }
        }

        /// <summary>
        /// 校验字节
        /// </summary>
        private byte _PackageParityByte = 0x00;
        public byte PackageParityByte
        {
            get { return _PackageParityByte; }
            set { _PackageParityByte = value; }
        }

        /// <summary>
        /// 完整数据包字节数组
        /// </summary>
        private byte[] _WholePackageDataBytes = null;
        public byte[] WholePackageDataBytes
        {
            get { return _WholePackageDataBytes; }
            set { _WholePackageDataBytes = value; }
        }

        /// <summary>
        /// 根据各个属性，获取完整数据包
        /// </summary>
        public void GetWholePackageDataBytes()
        {
            PackageLengthHighByte = (byte)((PackageDataBytes.Length >> 8) & 0xFF);
            PackageLengthLowByte = (byte)(PackageDataBytes.Length & 0xFF);
            PackageParityByte = (byte)(PackageCommandByte + PackageLengthLowByte + PackageLengthHighByte);
            foreach (byte cbyte in PackageDataBytes)
            {
                PackageParityByte = (byte)((PackageParityByte + cbyte) <= 255 ? (PackageParityByte + cbyte) : (PackageParityByte + cbyte) % 256);
            }

            WholePackageDataBytes = new byte[PackageDataBytes.Length + 6];
            WholePackageDataBytes[0] = PackageHeadByte;
            WholePackageDataBytes[1] = (byte)PackageCommandByte;
            WholePackageDataBytes[2] = PackageLengthLowByte;
            WholePackageDataBytes[3] = PackageLengthHighByte;
            Array.Copy(PackageDataBytes, 0, WholePackageDataBytes, 4, PackageDataBytes.Length);
            WholePackageDataBytes[4 + PackageDataBytes.Length] = PackageParityByte;
            WholePackageDataBytes[4 + PackageDataBytes.Length + 1] = PackageFootByte;
        }
    }
}
