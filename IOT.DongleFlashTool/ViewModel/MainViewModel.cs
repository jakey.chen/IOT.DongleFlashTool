﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOT.DongleFlashTool.ViewModel
{
    public class MainViewModel : Bandit.UI.MVVM.NotifyObject
    {
        /// <summary>
        /// 烧录文件路径
        /// </summary>
        private string _FirewarePath = string.Empty;
        public string FirewarePath
        {
            get { return _FirewarePath; }
            set
            {
                _FirewarePath = value;
                OnPropertyChanged("FirewarePath");
            }
        }

        /// <summary>
        /// OTA烧录文件路径
        /// </summary>
        private string _OTAFirewarePath = string.Empty;
        public string OTAFirewarePath
        {
            get { return _OTAFirewarePath; }
            set
            {
                _OTAFirewarePath = value;
                OnPropertyChanged("OTAFirewarePath");
            }
        }

        /// <summary>
        /// ICON
        /// </summary>
        private string _Icon = "Images/logo-blue.png";
        public string Icon
        {
            get { return _Icon; }
            set
            {
                _Icon = value;
                OnPropertyChanged("Icon");
            }
        }

        /// <summary>
        /// ShowInfo
        /// </summary>
        private string _ShowInfo = string.Empty;
        public string ShowInfo
        {
            get { return _ShowInfo; }
            set
            {
                _ShowInfo = value;
                OnPropertyChanged("ShowInfo");
            }
        }

        /// <summary>
        /// 进度条进度值
        /// </summary>
        private int _ProgressValue = 0;
        public int ProgressValue
        {
            get { return _ProgressValue; }
            set
            {
                _ProgressValue = value;
                OnPropertyChanged("ProgressValue");
                ProgressStr = string.Format("{0}%", ProgressValue);
            }
        }

        /// <summary>
        /// 进度条进度值显示
        /// </summary>
        private string _ProgressStr = String.Empty;
        public string ProgressStr
        {
            get { return _ProgressStr; }
            set
            {
                _ProgressStr = value;
                OnPropertyChanged("ProgressStr");
            }
        }

        /// <summary>
        /// Mac
        /// </summary>
        private string _Mac = String.Empty;
        public string Mac
        {
            get { return _Mac; }
            set
            {
                _Mac = value;
                OnPropertyChanged("Mac");
            }
        }

        /// <summary>
        /// CurrentDate
        /// </summary>
        private string _CurrentDate = String.Empty;
        public string CurrentDate
        {
            get { return _CurrentDate; }
            set
            {
                _CurrentDate = value;
                OnPropertyChanged("CurrentDate");
            }
        }

        /// <summary>
        /// 已添加的客户端(灯等设备)
        /// </summary>
        private Bandit.BanditCollection<AAA> _OnlineClients = new Bandit.BanditCollection<AAA>();
        public Bandit.BanditCollection<AAA> OnlineClients
        {
            get { return _OnlineClients; }
            set
            {
                _OnlineClients = value;
                OnPropertyChanged("OnlineClients");
            }
        }

        /// <summary>
        /// Endpoint
        /// </summary>
        private string _Endpoint = "FF";
        public string Endpoint
        {
            get { return _Endpoint; }
            set
            {
                _Endpoint = value;
                OnPropertyChanged("Endpoint");
            }
        }

        /// <summary>
        /// ManuCode
        /// </summary>
        private string _ManuCode = "11 68";
        public string ManuCode
        {
            get { return _ManuCode; }
            set
            {
                _ManuCode = value;
                OnPropertyChanged("ManuCode");
            }
        }

        /// <summary>
        /// ImageType
        /// </summary>
        private string _ImageType = "03 02";
        public string ImageType
        {
            get { return _ImageType; }
            set
            {
                _ImageType = value;
                OnPropertyChanged("ImageType");
            }
        }

        /// <summary>
        /// FileVersion
        /// </summary>
        private string _FileVersion = "11 34 03 02";
        public string FileVersion
        {
            get { return _FileVersion; }
            set
            {
                _FileVersion = value;
                OnPropertyChanged("FileVersion");
            }
        }
    }

    public class AAA : Bandit.UI.MVVM.NotifyObject
    {
        private string _Text;

        public string Text
        {
            get { return _Text; }
            set
            {
                _Text = value;
                OnPropertyChanged("Text");
            }
        }

        private string _Icon;

        public string Icon
        {
            get { return _Icon; }
            set
            {
                _Icon = value;
                OnPropertyChanged("Icon");
            }
        }

    }
}
